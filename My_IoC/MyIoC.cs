﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace My_IoC
{
    public class MyIoC
    {
        private Dictionary<Type, Type> typeTypeDictionary = new Dictionary<Type, Type>();
        public void RegisterType<TKey, TValue>()
        {
            var typeKey = typeof(TKey);
            var typeValue = typeof(TValue);
            typeTypeDictionary.Add(typeKey, typeValue);
        }
        //public TService Resolve<TService>()
        //{
        //    var valueType = typeTypeDictionary[typeof(TService)];
        //    return (TService)Activator.CreateInstance(valueType);
        //}
        public TService Resolve<TService>()
        {
            var valueType = typeTypeDictionary[typeof(TService)];
            return (TService)CreateType(valueType);
        }
        private object CreateType(Type type)
        {
            if (type.GetConstructors().Length > 1)
            {
                throw new Exception("Not support more then one constructor!");
            }
            var constructor = type.GetConstructors()[0];
            if (constructor.GetParameters().Length == 0)
            {
                return Activator.CreateInstance(type);
            }
            var args = new List<object>();
            foreach (var item in constructor.GetParameters())
            {
                var instance = typeTypeDictionary[item.ParameterType];
                var arg = CreateType(instance);
                args.Add(arg);
            }
            return Activator.CreateInstance(type, args.ToArray());
        }
    }
}
