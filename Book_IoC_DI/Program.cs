﻿using Autofac;
using Book_IoC_DI.BadPractis;
using Book_IoC_DI.GoodPractis;
using My_IoC;
using System;

namespace Book_IoC_DI
{
    class Program
    {
        public static MyIoC MyIoC { get; set; }
        static void Main(string[] args)
        {
            TryMyIoC();
            return;
            InvokeTeamServiceWithoutMyIoC();
            TryMyIoC();
            var user1 = new User();
            var user2 = Activator.CreateInstance(typeof(User));
            GoodPractisForUser();
            GoodPractisTest1();
            GoodPractisTest2();

        }
        static void InvokeTeamServiceWithoutMyIoC() //very sad practic without IoC
        {
            var importantService = new ImportantService();
            var phoneNumberService = new PhoneNumberService();
            var telephoneService = new GoodPractis.TelephoneService(phoneNumberService);
            var buildService = new GoodPractis.BuildService(telephoneService, importantService);
            var addressService = new GoodPractis.AddressService(buildService);
            var userService = new UserService(addressService);
            var teamService = new TeamService(userService, buildService, importantService);
            var (users, build) = teamService.GetById(new int[2] { 2, 3 }, 1);
            Console.WriteLine($"BuildTitle - {build.Title}, {build.Telephone.Number}");
        }
        static void TryMyIoC()
        {
            var myIoc = new MyIoC();
            MyIoC = myIoc;
            myIoc.RegisterType<ITeamService, GoodPractis.TeamService>();
            myIoc.RegisterType<ITelephoneService, GoodPractis.TelephoneService>();
            myIoc.RegisterType<IPhoneNumberService, GoodPractis.PhoneNumberService>();
            myIoc.RegisterType<IAddressService, GoodPractis.AddressService>();
            myIoc.RegisterType<IBuildService, GoodPractis.BuildService>();
            myIoc.RegisterType<IUserService, GoodPractis.UserService>();
            myIoc.RegisterType<IImportantService, GoodPractis.ImportantService>();
            //зарегестрировала телефон в myIoc
            var telephoneService = myIoc.Resolve<ITelephoneService>();
            var telephone = telephoneService.GetById(1);
            Console.WriteLine($"Telephone Id: {telephone.Id}, Number: {telephone.Number}");

            var addressService = myIoc.Resolve<IAddressService>();
            var address = addressService.GetById(23);
            Console.WriteLine($"Street: {address.Street}");

            var teamService = myIoc.Resolve<ITeamService>();
            var (users, build) = teamService.GetById(new int[2] { 2, 3 }, 1);
            foreach (var item in users)
            {
                Console.WriteLine($"UserName: {item.Name}");
            }
            Console.WriteLine($"BuildTitle: {build.Title}");
        }
        static void GoodPractisForUser()
        {
            // init IoC once time
            var builder = new ContainerBuilder();
            builder.RegisterType<GoodPractis.TelephoneService>().As<ITelephoneService>();
            builder.RegisterType<GoodPractis.BuildService>().As<IBuildService>();
            builder.RegisterType<GoodPractis.AddressService>().As<IAddressService>();
            builder.RegisterType<UserService>().As<IUserService>();
            var container = builder.Build();
            // 
            var userService = container.Resolve<IUserService>();

            var result = userService.GetById(2);
            Console.WriteLine(result.Id);
        }
        static void GoodPractisForUser__0_0()
        {
            var phoneNumberService = new PhoneNumberService();
            var telephoneService = new GoodPractis.TelephoneService(phoneNumberService);
            var buildService = new GoodPractis.BuildService(telephoneService, new ImportantService());
            var addressService = new GoodPractis.AddressService(buildService);
            var userService = new UserService(addressService);
            var result = userService.GetById(2);
            Console.WriteLine(result.Id);
        }
        static void GoodPractisTest2()
        {
            var addressMock = new AddressMockService();
            var userService = new UserService(addressMock);
            var result = userService.GetById(2);
            if (result.Id == 2 && result.Address.Id == 2)
            {
                Console.WriteLine("Test is ok!");
            }
            else
            {
                throw new Exception("Test failed");
            }
        }
        static void GoodPractisTest1()
        {
            var bbbMock = new BbbMock();
            var aaa = new Aaa(bbbMock);
            var result = aaa.CalcAdd(1, 1);
            if (result == 300)
            {
                Console.WriteLine("Test is ok!");
            }
            else
            {
                throw new Exception("Test failed");
            }
        }

        static void GoodPractis()
        {
            var ccc = new Ccc();
            var bbb = new Bbb(ccc);
            var aaa = new Aaa(bbb);
            aaa.Use();
        }

        static void BadPractisTwo()
        {
            var bb = new Bb();
            var aa = new Aa(bb);
            aa.Use();
        }

        static void BadPractis()
        {
            var a = new A();
            a.Use();
        }
    }
}
