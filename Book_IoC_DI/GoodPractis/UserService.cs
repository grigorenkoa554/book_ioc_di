﻿using System;
using System.Linq;

namespace Book_IoC_DI.GoodPractis
{
    public interface ITeamService
    {
        (User[], Build) GetById(int[] ids, int buildId);
    }
    public class TeamService : ITeamService
    {
        private readonly IUserService userService;
        private readonly IBuildService buildService;
        private readonly IImportantService importantService;

        public TeamService(IUserService userService,
            IBuildService buildService, IImportantService importantService)
        {
            this.userService = userService;
            this.buildService = buildService;
            this.importantService = importantService;
        }
        public (User[], Build) GetById(int[] ids, int buildId)
        {
            // context
            importantService.DoSomeImportant();
            var users = ids.Select(id => userService.GetById(id)).ToArray();
            var build = buildService.GetById(buildId);

            return (users, build);
        }
    }
    public interface IUserService
    {
        User GetById(int id);
    }
    public class UserService : IUserService
    {
        private readonly IAddressService addressService;

        public UserService(IAddressService addressService)
        {
            this.addressService = addressService;
        }
        public User GetById(int id)
        {
            // context
            var item = new User { Id = id, Name = "Iurii" };
            var child = addressService.GetById(id);
            item.Address = child;
            return item;
        }
    }
    public interface IAddressService
    {
        Address GetById(int id);
    }
    public class AddressService : IAddressService
    {
        private readonly IBuildService buildService;

        public AddressService(IBuildService buildService)
        {
            this.buildService = buildService;
        }
        public Address GetById(int id)
        {
            // context
            var item = new Address { Id = id, Street = "Golovackogo 53/21" };
            var child = buildService.GetById(id);
            item.Build = child;

            return item;
        }
    }
    public interface IBuildService
    {
        Build GetById(int id);
    }
    public class BuildService : IBuildService
    {
        private readonly ITelephoneService telephoneService;
        private readonly IImportantService importantService;

        public BuildService(ITelephoneService telephoneService,
            IImportantService importantService)
        {
            this.telephoneService = telephoneService;
            this.importantService = importantService;
        }
        public Build GetById(int id)
        {
            // context
            importantService.DoSomeImportant();
            var item = new Build { Id = id, Title = "Old build" };
            var child = telephoneService.GetById(id);
            item.Telephone = child;

            return item;
        }
    }
    public interface ITelephoneService
    {
        Telephone GetById(int id);
    }
    public class TelephoneService : ITelephoneService
    {
        private readonly IPhoneNumberService phoneNumberService;

        public TelephoneService(IPhoneNumberService phoneNumberService)
        {
            this.phoneNumberService = phoneNumberService;
        }
        public Telephone GetById(int id)
        {
            // context
            var item = new Telephone
            {
                Id = id,
                Number = phoneNumberService.Generate()
            };


            return item;
        }
    }

    public interface IPhoneNumberService
    {
        string Generate();
    }
    public class PhoneNumberService : IPhoneNumberService
    {
        public string Generate()
        {
            Console.WriteLine("Generate number");
            return "+375668332341";
        }
    }

    public interface IImportantService
    {
        void DoSomeImportant();
    }
    public class ImportantService : IImportantService
    {
        public void DoSomeImportant()
        {
            Console.WriteLine("Print some important logic!!!");
        }
    }

    public class AddressMockService : IAddressService
    {
        public Address GetById(int id)
        {
            // context
            var item = new Address
            {
                Id = id,
                Street = "Golovackogo 53/21",
                Build = new Build
                {
                    Id = id,
                    Title = "Good street",
                    Telephone = new Telephone { Id = id, Number = "+13425346" }
                }
            };

            return item;
        }
    }
}
