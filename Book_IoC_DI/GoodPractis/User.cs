﻿namespace Book_IoC_DI.GoodPractis
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Address Address { get; set; }
    }
    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }

        public Build Build { get; set; }
    }
    public class Build
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public Telephone Telephone { get; set; }
    }
    public class Telephone
    {
        public int Id { get; set; }
        public string Number { get; set; }
    }
}
