﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book_IoC_DI.GoodPractis
{
    public class Aaa
    {
        private readonly IBbb bbb;

        public Aaa(IBbb bbb)
        {
            this.bbb = bbb;
        }
        public void Use()
        {
            bbb.Print();
        }
        public int CalcAdd(int x, int y)
        {
            var result = bbb.CalcAdd(x, y);
            return result + 100;
        }
    }

    public interface IBbb
    {
        int CalcAdd(int x, int y);
        void Print();
    }

    public class Bbb : IBbb
    {
        private readonly ICcc ccc;

        public Bbb(ICcc ccc)
        {
            this.ccc = ccc;
        }

        public int CalcAdd(int x, int y)
        {
            return ccc.CalcAdd(x, y);
        }

        public void Print()
        {
            Console.WriteLine("Print B");
            ccc.Print();
        }
    }

    public interface ICcc
    {
        int CalcAdd(int x, int y);
        void Print();
    }

    public class Ccc : ICcc
    {
        public int CalcAdd(int x, int y)
        {
            // invoke web
            return x + y;
        }

        public void Print()
        {
            Console.WriteLine("Print C");
        }
    }
    // ...

    public class BbbMock : IBbb
    {
        public int CalcAdd(int x, int y)
        {
            return 200;
        }

        public void Print()
        {
        }
    }
}
