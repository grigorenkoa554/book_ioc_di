﻿using System;

namespace Book_IoC_DI.BadPractis
{
    public class A
    {
        public void Use()
        {
            var b = new B();
            b.Print();
        }
    }

    public class B
    {
        public void Print()
        {
            Console.WriteLine("Print B");
        }
    }
}
