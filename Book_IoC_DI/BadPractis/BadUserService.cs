﻿using Book_IoC_DI.GoodPractis;

namespace Book_IoC_DI.BadPractis
{
    public class BadUserService
    {
        public User GetById(int id)
        {
            // context
            var item = new User { Id = id, Name = "Iurii" };
            var service = new AddressService();
            var child = service.GetById(id);
            item.Address = child;
            return item;
        }
    }
    public class AddressService
    {
        public Address GetById(int id)
        {
            // context
            var item = new Address { Id = id, Street = "Golovackogo 53/21" };
            var service = new BuildService();
            var child = service.GetById(id);
            item.Build = child;

            return item;
        }
    }
    public class BuildService
    {
        public Build GetById(int id)
        {
            // context
            var item = new Build { Id = id, Title = "Old build" };
            var service = new TelephoneService();
            var child = service.GetById(id);
            item.Telephone = child;

            return item;
        }
    }
    public class TelephoneService
    {
        public Telephone GetById(int id)
        {
            // context
            var item = new Telephone { Id = id, Number = "+47562732" };

            return item;
        }
    }
}
