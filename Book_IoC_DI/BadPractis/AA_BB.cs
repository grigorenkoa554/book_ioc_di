﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Book_IoC_DI.BadPractis
{
    public class Aa
    {
        private readonly Bb bb;

        public Aa(Bb bb)
        {
            this.bb = bb;
        }
        public void Use()
        {
            bb.Print();
        }
    }

    public class Bb
    {
        public void Print()
        {
            Console.WriteLine("Print B");
        }
    }
}
